window.onload = function () {
    init();
};

var user_name = '';
var user_email = '';
var user_id = '';
var user_verified = false;

function init() {
    initUserInfo();
    initForumContent();
}

function initUserInfo() {
    const logIn = '<a class="nav-link dropdown-toggle" href="login.html" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i> Log in </a>';
    const userAccount = '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i>';
    const dropMenu = '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">';
    const proFile = '<a class="dropdown-item" href="userpage.html">Profile</a>';
    const setting = '<a class="dropdown-item" href="setting.html">Setting</a>';
    const divider = '<div class="dropdown-divider"></div>';
    const logOut = '<a class="dropdown-item" id="logout" href="#">Log out</a>';

    firebase.auth().onAuthStateChanged(user => {
        let dropDown = document.getElementById('dropdown');
        if (user) {
            user_name = (user.displayName)? user.displayName : 'N/A';
            user_email = user.email;
            user_id = `${user_name}(${user_email})`;
            user_verified = user.emailVerified;
            dropDown.innerHTML = userAccount + ` ${user_id} </a>` +
                                 dropMenu +
                                 proFile +
                                 setting +
                                 divider +
                                 logOut +
                                 '</div>';
            let btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', () => {
                firebase.auth().signOut().then(() => {
                    alert('Log out success!');
                }).catch(() => {
                    alert('error');
                })
            });
        } else {
            dropDown.innerHTML = logIn;
            let info = document.getElementById('loginfo');
            let forum = document.getElementById('forum');    
            let discuss = document.getElementById('discuss');
            info.classList.remove('d-none');
            forum.classList.add('d-none');
            discuss.classList.add('d-none');
        }
    });
}

function initForumContent() {
    const buttonFront = '<button class="btn btn-primary btn-md mx-1 my-1" role="button"';

    let forumRef = firebase.database().ref('forum');
    let forum = document.getElementById('forum');
    let forumButtons = document.getElementById('buttons');
    let discuss = document.getElementById('discuss');
    let buttons = [];
    
    forum.classList.remove('d-none');
    discuss.classList.add('d-none');

    forumRef.once('value')
        .then(snapshot => {
            snapshot.forEach(snap => {
                let title = snap.key;
                let newButton = `${buttonFront} id="${title}">${title}</button>`;
                buttons.push(newButton);
            });
            forumButtons.innerHTML = buttons.join('');
            let btn = forumButtons.childNodes;
            btn.forEach(b => {
                b.addEventListener('click', changeForum.bind(null, b.id));
            })
        })
        .catch(e => console.log(e.message));
}

function changeForum(forumId) {
    const postFront = '<a class="title ml-3" href="#"';

    let forumRef = firebase.database().ref(`forum/${forumId}`);
    let postList = document.getElementById('postlist');
    let posts = [];
    var firstCount = 0;
    var secondCount = 0;

    // init forum content
    forumRef.once('value')
        .then(snapshot => {
            snapshot.forEach(snap => {
                let postId = snap.key;
                let data = snap.val();
                let postTitle = data.topic;
                let user = data.floor_1.post.user;
                let newPost =   `<div class="post-item d-flex flex-column flex-sm-row justify-content-between rounded">
                                    ${postFront} id="${postId}">${postTitle}</a>
                                    <span class="author ml-3 ml-sm-0 mr-sm-3">${user}</span>
                                 </div>`;
                posts.push(newPost);
                firstCount++;
            });
            postList.innerHTML = posts.join('<hr class="m-0">');

            forumRef.on('child_added', value => {
                secondCount++;
                if (secondCount > firstCount) {
                    let postId = value.key;
                    let data = value.val();
                    let postTitle = data.topic;
                    let user = data.floor_1.post.user;
                    let newPost =   `<div class="post-item d-flex flex-column flex-sm-row justify-content-between rounded">
                                        ${postFront} id="${postId}">${postTitle}</a>
                                        <span class="author ml-3 ml-sm-0 mr-sm-3">${user}</span>
                                    </div>`;
                    posts.push(newPost);
                    postList.innerHTML = posts.join('<hr class="m-0">');
                    let links = document.querySelectorAll('.title');
                    links.forEach(l => {
                        l.addEventListener('click', viewPost.bind(null, forumId, l.id, l.textContent));
                    });
                }
            });

            let links = document.querySelectorAll('.title');
            links.forEach(l => {
                l.addEventListener('click', viewPost.bind(null, forumId, l.id, l.textContent));
            });

            let create = document.getElementById('create-button');
            let newTitle = document.getElementById('create-title');
            let newContent = document.getElementById('create-content');
            let cancel = document.getElementById('cancel-new-post');
            let submit = document.getElementById('create-new-post');
            createBlockCollapse();
            create.addEventListener('click', () => {
                if (user_verified)
                    createBlockExpand();
                else
                    create_alert('error', 'Please verify your email account first to leave messages!');
            });
            cancel.addEventListener('click', () => {
                createBlockCollapse();
            });
            submit.onclick = function() {
                createBlockCollapse();
                return createNewPost(newTitle, newContent, forumRef);
            };
        })
        .catch(e => console.log(e.message));
}

function viewPost(forumId, postId, title) {
    let postRef = firebase.database().ref(`forum/${forumId}/${postId}`);
    let forum = document.getElementById('forum');
    let discuss = document.getElementById('discuss');
    let prev = document.getElementById('go-back');
    let topic = document.getElementById('topic');
    let buildList = document.getElementById('build-list');
    let builds = [];
    var firstCount = 0;
    var secondCount = 0;

    createBlockCollapse();
    forum.classList.add('d-none');
    discuss.classList.remove('d-none');
    topic.textContent = title;

    prev.addEventListener('click', () => {
        initForumContent();
        changeForum.bind(null, forumId);
        buildList.innerHTML = '';
    });

    // init post content
    postRef.once('value')
        .then(snapshot => {
            snapshot.forEach(snap => {
                if (snap.key !== 'topic') {
                    builds.push(loadPost(snap));
                    firstCount++;
                }
            })
            buildList.innerHTML = builds.join('<hr>');

            postRef.on('child_added', value => {
                if (value.key !== 'topic') {
                    secondCount++;
                    if (secondCount > firstCount) {
                        buildList.innerHTML += `<hr>${loadPost(value)}`;
                        addPostListeners(forumId, postId);
                    }
                }
            });
            addPostListeners(forumId, postId);
        })
        .catch(e => console.log(e.message));
}

function loadPost(snap) {
    const buildFront = '<div class="build shadow-lg"'
    const tagFront = '<div class="tag d-flex flex-column p-3">';
    const floorFront = '<h3 class="my-2">';
    const floorEnd = '</h3>';
    const authorFront = '<h5 class="my-2">';
    const authorEnd = '</h5>';
    const likeFront = `<div class="likes mr-sm-3 my-auto">
                        <i class="far fa-thumbs-up"></i><span id="like-count">  `;
    const likeEnd = '</span></div>';
    const tagEnd = '</div>';
    const textFront = '<div class="content p-3">';
    const textEnd = '</div>';
    const commentListFront = '<div class="comment-list p-3"><h5 class="my-2">Comment:</h5>';
    const commentListEnd = '</div>';
    const commentZone = `<div class="form-group p-3 d-none">
                            <div class="mx-auto">
                                <textarea class="form-control" rows="2" placeholder="comment here!"></textarea>
                            </div>
                            <div class="d-table ml-auto mt-2">
                                <button class="submit-button btn btn-success btn-md mx-1 my-1" id="submit-comment" role="button">Submit</button>
                            </div>
                         </div>`;
    const replyFront = '<div class="reply d-flex flex-column flex-sm-row justify-content-end p-3">';
    const replyButtonFront = '<a class="reply-button btn btn-warning btn-md ml-auto mx-sm-1 my-1" href="#new-build" role="button"';
    const replyButtonEnd = '</a>';
    const otherButtonFront = '<button class="reply-button btn btn-warning btn-md ml-auto mx-sm-1 my-1" role="button"';
    const otherButtonEnd = '</button>';
    const replyEnd = '</div>';
    const buildEnd = '</div>';

    let data = snap.val();
    let floor = Number(snap.key.match(/\d+/));
    let floor_str = '';
    if (floor === 1)
        floor_str = `${floorFront}Host${floorEnd}`;
    else
        floor_str = `${floorFront}#${floor}${floorEnd}`;
    let post = data.post;
    let like = data.like;
    let author = post.user;
    let content = post.text;
    let likes = like.likeCount;
    let commentList = (data.comments)? data.comments : null;
    let comments = [];
    if (commentList) {
        for (let c of Object.values(commentList)) {
            let comment_str = `<div class="comment-item d-flex flex-column flex-sm-row justify-content-between rounded">
                                <span class="text ml-3">${c.text}</span>
                                <span class="author ml-3 ml-sm-0 mr-sm-3">by ${c.user}</span>
                               </div>`;
            comments.push(comment_str);
        }
    }
    let total_str = comments.join('<hr class="comment-divide m-0">');
    let newBuild = `${buildFront} id="floor_${floor}">
                        ${tagFront}
                            ${floor_str}
                            <div class="d-flex flex-column flex-sm-row justify-content-between">
                                ${authorFront}<i class="fas fa-user"></i> ${author}${authorEnd}
                                ${likeFront}${likes}${likeEnd}
                            </div>
                        ${tagEnd}
                        ${textFront}
                            ${content}
                        ${textEnd}
                        ${commentListFront}
                            ${total_str}
                        ${commentListEnd}
                        ${commentZone}
                        <hr class="comment-button-divide m-0">
                        ${replyFront}
                            ${replyButtonFront} id="reply">
                                reply
                            ${replyButtonEnd}
                            ${otherButtonFront} id="comment">
                                comment
                            ${otherButtonEnd}
                            ${otherButtonFront} id="like">
                                like
                            ${otherButtonEnd}
                        ${replyEnd}
                    ${buildEnd}`;
    return newBuild;
}

function addPostListeners(forumId, postId) {
    let b = document.querySelectorAll('.build');
    let postRef = firebase.database().ref(`forum/${forumId}/${postId}`);
    var total_floor = 0;
    b.forEach(instance => {
        let num = Number(instance.querySelector('h3').textContent.match(/\d+/));
        num = (num === 0)? 1 : num;
        total_floor = num;
        let floor = `floor_${num}`;
        let buildRef = postRef.child(`${floor}`);
        let likeRef = buildRef.child('like');
        let cZone = instance.querySelector('.form-group');
        let commentInput = instance.querySelector('.form-control');
        let likeCount = instance.querySelector('#like-count');
        let commentButton = instance.querySelector('#comment');
        let submitCommentButton = instance.querySelector('#submit-comment');
        let likeButton = instance.querySelector('#like');

        commentButton.addEventListener('click', () => {
            commentInput.value = '';
            cZone.classList.toggle('d-none');
        });
        submitCommentButton.addEventListener('click', () => {
            if (user_verified) {
                if (strip(commentInput.value))
                    cZone.classList.toggle('d-none');
                return submitComment(commentInput, buildRef);
            } else
                create_alert('error', 'Please verify your email account first to leave messages!');
        });
        likeButton.addEventListener('click', () => {
            return submitLike(likeCount, likeRef);
        });

        let commentRef = buildRef.child('comments');
        let commentList = instance.querySelector('.comment-list');
        let comments = [];
        
        commentRef.on('child_added', value => {
            let c = (value.val())? value.val() : null;
            let comment_str = `<div class="comment-item d-flex flex-column flex-sm-row justify-content-between rounded">
                                <span class="text ml-3">${c.text}</span>
                                <span class="author ml-3 ml-sm-0 mr-sm-3">by ${c.user}</span>
                               </div>`;
            comments.push(comment_str);
            let total_str = comments.join('<hr class="comment-divide m-0">');
            commentList.innerHTML = `<h5 class="my-2">Comment:</h5>${total_str}`;
        });

        likeRef.on('child_changed', value => {
            let l = value.val();
            if (typeof(l) === 'number') {
                likeCount.innerHTML = `  ${l}`;
            }
        });
    });

    let submitReplyButton = document.getElementById('submit-reply');
    let replyInput = document.getElementById('reply-input');
    submitReplyButton.onclick = function() {
        if (user_verified)
            submitReply(replyInput, total_floor, postRef);
        else
            create_alert('error', 'Please verify your email account first to leave messages!');
    };
}

function createNewPost(titleInput, contentInput, forumRef) {
    let title = strip(titleInput.value);
    let content = strip(contentInput.value);
    if (title && content) {
        let newData = {
            topic: title,
            floor_1: {
                post: {
                    text: content,
                    user: user_email
                },
                like: {
                    likeCount: 0
                }
            }
        }
        let newPostKey = forumRef.push().key;
        let updates = {};
        updates[`/${newPostKey}`] = newData;
        titleInput.value = '';
        contentInput.value = '';
        return forumRef.update(updates);
    }
}

function submitReply(input, floors, postRef) {
    let text = strip(input.value);
    if (text) {
        let newData = {
            post: {
                text: text,
                user: user_email
            },
            like: {
                likeCount: 0
            }
        };
        let updates = {};
        updates[`/floor_${floors + 1}`] = newData;
        input.value = '';
        return postRef.update(updates);
    }
}

function submitComment(input, buildRef) {
    let text = strip(input.value);
    if (text) {
        let newData = {
            text: text,
            user: user_email
        };
        let newPostKey = buildRef.push().key;
        let updates = {};
        updates['/comments/' + newPostKey] = newData;
        input.value = '';
        return buildRef.update(updates);
    }
}

function submitLike(likeCount, likeRef) {
    let usersRef = likeRef.child('likedUsers').orderByValue().equalTo(`${user_email}`);
    usersRef.once('value')
        .then(snapshot => {
            let data = snapshot.val();
            if (!data) {
                let like = Number(likeCount.textContent);
                like++;
                let likeData = like;
                let updates = {};
                updates['/likeCount'] = likeData;
                updates[`/likedUsers/${like - 1}`] = user_email;
                return likeRef.update(updates);
            } else {
                create_alert('error', 'You have already clicked like on this post!');
            }
        })
        .catch(e => console.log(e.message));
}

function createBlockExpand() {
    let create = document.getElementById('create-button');
    let createBlock = document.getElementById('new-post');
    create.classList.add('d-none');
    createBlock.classList.remove('d-none');
}

function createBlockCollapse() {
    let create = document.getElementById('create-button');
    let createBlock = document.getElementById('new-post');
    create.classList.remove('d-none');
    createBlock.classList.add('d-none');
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

function strip(originalString) {
    var strippedString = originalString.replace(/(<([^>]+)>)/ig,"");
    return strippedString;
}