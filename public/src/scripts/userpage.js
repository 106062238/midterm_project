window.onload = function () {
    init();
};

function init() {
    const logIn = '<a class="nav-link dropdown-toggle" href="login.html" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i> Log in </a>';
    const userAccount = '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i>';
    const dropMenu = '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">';
    const proFile = '<a class="dropdown-item" href="userpage.html">Profile</a>';
    const setting = '<a class="dropdown-item" href="setting.html">Setting</a>';
    const divider = '<div class="dropdown-divider"></div>';
    const logOut = '<a class="dropdown-item" id="logout" href="#">Log out</a>';

    firebase.auth().onAuthStateChanged(user => {
        var dropDown = document.getElementById('dropdown');
        if (user) {
            var user_name = (user.displayName)? user.displayName : 'N/A';
            var user_email = user.email;
            var user_id = `${user_name}(${user_email})`;
            var user_uid = user.uid;
            var user_emailVerified = (user.emailVerified)? 'Yes' : 'No';
            
            dropDown.innerHTML = userAccount + ` ${user_id} </a>` +
                                 dropMenu +
                                 proFile +
                                 setting +
                                 divider +
                                 logOut +
                                 '</div>';
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', () => {
                firebase.auth().signOut().then(() => {
                    alert('Log out success!');
                }).catch(() => {
                    alert('error');
                })
            });
            
            var name = document.getElementById('user-name');
            var email = document.getElementById('user-email');
            var uid = document.getElementById('user-uid');
            var verified = document.getElementById('user-verified');
            var btnVerify = document.getElementById('verify-button');
            name.textContent = user_name;
            email.textContent = user_email;
            uid.textContent = user_uid;
            verified.textContent = user_emailVerified;
            if (user.emailVerified) {
                btnVerify.classList.add('d-none');
            } else {
                btnVerify.addEventListener('click', () => {
                    user.sendEmailVerification().then(() => {
                        create_alert('success', 'Your email verification has been sent, please check it in your email! If you are not receiving it in 5 minutes, click the button again!');
                    }).catch(e => console.log(e.message));
                });
            }
        } else {
            dropDown.innerHTML = logIn;
            let info = document.getElementById('loginfo');
            let userProfile = document.getElementById('user-profile');
            info.classList.remove('d-none');
            userProfile.classList.add('d-none');
        }
    });
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}
