window.onload = function () {
    init();
};

function init() {
    const logIn = '<a class="nav-link dropdown-toggle" href="login.html" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i> Log in </a>';
    const userAccount = '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i>';
    const dropMenu = '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">';
    const proFile = '<a class="dropdown-item" href="userpage.html">Profile</a>';
    const setting = '<a class="dropdown-item" href="setting.html">Setting</a>';
    const divider = '<div class="dropdown-divider"></div>';
    const logOut = '<a class="dropdown-item" id="logout" href="#">Log out</a>';
    
    firebase.auth().onAuthStateChanged(user => {
        var dropDown = document.getElementById('dropdown');
        if (user) {
            var user_name = (user.displayName)? user.displayName : 'N/A';
            var user_email = user.email;
            var user_id = `${user_name}(${user_email})`;

            dropDown.innerHTML = userAccount + ` ${user_id} </a>` +
                                 dropMenu +
                                 proFile +
                                 setting +
                                 divider +
                                 logOut +
                                 '</div>';
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', () => {
                firebase.auth().signOut().then(() => {
                    alert('Log out success!');
                }).catch(() => {
                    alert('error');
                })
            });

            var name = document.getElementById('user-name');
            var password = document.getElementById('new-password');
            var confirmation = document.getElementById('confirmation');
            var btnSubmit = document.getElementById('submit-setting');

            btnSubmit.addEventListener('click', () => {
                let n = strip(name.value);
                let p = password.value;
                let c = confirmation.value;
                if (n && p && c) {
                    if (p === c) {
                        user.updateProfile({
                            displayName: n,
                            photoURL: ''
                        })
                        .then(() => {
                            user.updatePassword(p)
                            .then(() => {
                                create_alert('success', 'Update succeed!');
                            })
                            .catch(error => {
                                var errorCode = error.code;
                                var errorMessage = error.message;
                                var email = error.email;
                                var credential = error.credential;
                                create_alert('error', errorMessage);
                            })
                        })
                        .catch(error => {
                            var errorCode = error.code;
                            var errorMessage = error.message;
                            var email = error.email;
                            var credential = error.credential;
                            create_alert('error', errorMessage);
                        })
                        
                    } else {
                        create_alert('error', 'Your password is inconsistent!');
                    }
                } else {
                    create_alert('error', 'Fill out all the form!');
                }
            });
        } else {
            dropDown.innerHTML = logIn;
            let info = document.getElementById('loginfo');
            let userSetting = document.getElementById('user-setting');
            info.classList.remove('d-none');
            userSetting.classList.add('d-none');
        }
    });
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

function strip(originalString) {
    var strippedString = originalString.replace(/(<([^>]+)>)/ig,"");
    return strippedString;
}