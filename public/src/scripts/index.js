window.onload = function () {
    init();
};

const logIn = '<a class="nav-link dropdown-toggle" href="login.html" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i> Log in </a>';
const userAccount = '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i>';
const dropMenu = '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">';
const proFile = '<a class="dropdown-item" href="userpage.html">Profile</a>';
const setting = '<a class="dropdown-item" href="setting.html">Setting</a>';
const divider = '<div class="dropdown-divider"></div>';
const logOut = '<a class="dropdown-item" id="logout" href="#">Log out</a>';

function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(user => {
        var dropDown = document.getElementById('dropdown');
        var start = document.getElementById('start');
        if (user) {
            var user_name = (user.displayName)? user.displayName : 'N/A';
            user_email = user.email;
            var user_id = `${user_name}(${user_email})`;
            dropDown.innerHTML = userAccount + ` ${user_id} </a>` +
                                 dropMenu +
                                 proFile +
                                 setting +
                                 divider +
                                 logOut +
                                 '</div>';
            var btnLogout = document.getElementById('logout');
            btnLogout.addEventListener('click', () => {
                firebase.auth().signOut().then(() => {
                    alert('Log out success!');
                }).catch(() => {
                    alert('error');
                })
            });
            start.href = 'forum.html';
        } else {
            dropDown.innerHTML = logIn;
            start.href = 'login.html';
        }
    });
}