window.onload = function () {
    init();
};

function init() {
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var remember = document.getElementById('remember');

    btnLogin.addEventListener('click', function () {
        if (remember.checked) {
            firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(() => {
                return firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(() => {
                    window.location = 'forum.html';
                }).catch(error => {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    create_alert("error", errorMessage);
                    txtEmail.value = '';
                    txtPassword.value = '';
                });
            }).catch(error => {
                var errorCode = error.code;
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = '';
                txtPassword.value = '';
            })
        } else {
            firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(() => {
                window.location = 'forum.html';
            }).catch(error => {
                var errorCode = error.code;
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = '';
                txtPassword.value = '';
            });
        }
    });

    btnGoogle.addEventListener('click', () => {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(result => {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location = 'forum.html';
        }).catch(error => {
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
            create_alert('error', errorMessage);
        });
    });

    btnSignUp.addEventListener('click', function () {        
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(() => {
            create_alert('success', 'Account created!');
        }).catch(error => {
            var errorCode = error.code;
            var errorMessage = error.message;
            create_alert('error', errorMessage);
        });
        txtEmail.value = '';
        txtPassword.value = '';
    });
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}