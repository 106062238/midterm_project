# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : !@#$%^&* Forum
* Key functions (add/delete)
    1. user page
    2. post page (add)
    3. post list page (add)
    4. leave comment under any post (add)
    
* Other functions (add/delete)
    1. like the post
    2. view limitation
    3. post limitation
    4. comment limitation
    5. setting users name and new password
    6. password setting consistency check
    7. verification(email)
    8. remember me

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-project-e328b.firebaseapp.com

# Components Description : 
1. user page : users are able to check their profiles after logged in
2. post page (add) : verified users are able to create a new post(reply) in every post
3. post list page (add) : verified users are able to create a new post(first post) on the post list of every forum page
4. leave comment under any post (add) : verified users are able to leave a comment under every post

# Other Functions Description(1~10%) : 
1. like the post : once per user account in every post
2. view limitation : only for logged in users
3. post limitation : only for verified users
4. comment limitation : only for verified users
5. setting users name and new password : from setting page of logged in users
6. password setting consistency check : password double checking when update new password
7. verification(email) : could be sent from profiles of unverified users
8. remember me : account will remain logged in even after the tab is closed, the account is logged out until the window is closed

## Security Report (Optional)
    strip the string obtained from input field if it contains any html tags.